import React, { Component } from 'react';
import _ from 'lodash';
import { Search, Grid, Header, Segment, Item  } from 'semantic-ui-react';
import './App.css';
import source from './clients.json';

// The Components for show clients list
const ClientList = (props) => props.data.map((client, k)=>{        
    return (
      <Item key={k}>
        <Item.Image size='tiny' src={client.general.avatar} />
        <Item.Content>
          <Item.Header as='a'>{client.general.firstName + ' ' + client.general.lastName}</Item.Header>
          <Item.Description>
            {client.job.title}
          </Item.Description>
        </Item.Content>
    </Item>
    )
})

export default class App extends Component 
{
    componentWillMount() {
        this.resetComponent()
    }

    // Reset data filtered function
    resetComponent = () => this.setState({ isLoading: false, results: source, value: '' })

    //Function filtered clients
    handleSearchChange = (e, { value }) => 
    {
        this.setState({ isLoading: true, value })

        setTimeout(() => 
        {
            if (this.state.value.length < 1) return this.resetComponent()

            const re = new RegExp(_.escapeRegExp(this.state.value), 'i')

            const isMatch = result => {
                return re.test(JSON.stringify(result))
            }
            
            let data = _.filter(source, isMatch);

            if(!data){
              data = source;
            }
            
            this.setState({
              isLoading: false,
              results:data
            })
        }, 300)
    }

    render() {
      const { isLoading, value } = this.state
  
      return (
        <Grid>
          <Grid.Column width={6}>
            <Search
              loading={isLoading}
              onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
              value={value}
              {...this.props}
            />
            <Item.Group>
                <ClientList data={this.state.results} />
            </Item.Group>
          </Grid.Column>
          <Grid.Column width={10}>
            <Segment>
              <Header>State</Header>
              <pre style={{ overflowX: 'auto' }}>{JSON.stringify(this.state, null, 2)}</pre>
              <Header>Options</Header>
              <pre style={{ overflowX: 'auto' }}>{JSON.stringify(source, null, 2)}</pre>
            </Segment>
          </Grid.Column>
        </Grid>
      )
    }
}
